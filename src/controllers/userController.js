const mongoose = require('mongoose');
const User = mongoose.model('User');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const env = require('dotenv').config().parsed;
const utils = require('../utils');
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'scalengeng@gmail.com',
    pass: 'NgMb1988T$'
  }
});

// Register
exports.register = (req, res) => {
  User.create({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  }, function (error, user) {
    if (error) {
      console.log(error);
      return res.status(400).json(error);
    }

    jwt.sign(
      {
        user: {
          id: user.id
        }
      },
      env.APP_SECRET, {
        expiresIn: 10800 // 3h
      },
      (error, token) => {
        if (error) {
          console.log(error);
          return res.status(500).json(error);
        }

        let mailOptions = {
          from: 'scalenge-noreply@gmail.com',
          to: 'tomasz.ngondo@gmail.com',
          subject: 'Registration',
          text: req.body.emailMessage.replace('{token}', token).replace('{username}', req.body.username)
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            return res.status(500).json(error);
          }

          return res.status(200).json({
            message: 'Email sent: ' + info.response,
            name: 'RegistrationSuccess'
          });
        });
      }
    );
  });
};

// Register confirm
exports.registerConfirm = (req, res) => {
  let token = utils.retrieveToken(res, req.headers.authorization);

  try {
    let decoded = jwt.verify(token, env.APP_SECRET);

    User.findOneAndUpdate({
      _id: decoded.user.id
    }, req.body, (error, user) => {
      if (error) {
        console.log(error);
        return res.status(500).json(error);
      }

      if (user.verified === req.body.verified) {
        console.log('Registration Error: User already verified');
        return res.status(400).json({
          errors: {
            register: {
              message: 'User already verified',
              name: 'RegistrationError'
            }
          }
        });
      }

      jwt.sign(
        {
          user: {
            id: user.id,
            username: user.username
          }
        },
        env.APP_SECRET,
        {
          expiresIn: 10800 // 3h
        },
        (error, token) => {
          if (error) {
            console.log(error);
            return res.status(500).json(error);
          }

          return res.status(200).json({
            message: 'User registration confirmed successfully',
            name: 'RegistrationConfirmSuccess',
            token
          });
        }
      );
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: {
        tokenExpired: true,
        message: error
      }
    });
  }
};

// Login
exports.login = (req, res) => {
  User.findOne({
    email: req.body.email
  }, function(error, user) {
    if (error) {
      console.log(error);
      return res.status(500).json(error);
    }

    if (!user) {
      console.log('Login Error: Invalid e-mail');
      return res.status(400).json({
        errors: {
          email: {
            message: 'validation.email.invalid',
            name: 'ValidationError'
          }
        }
      });
    }

    user.validatePassword(req.body.password, user.password, function(error, isMatch) {
      if (error) {
        console.log(error);
        return res.status(500).json(error);
      }

      if (!isMatch) {
        console.log('Login Error: Password is invalid');
        return res.status(400).json({
          errors: {
            password: {
              message: 'validation.password.invalid',
              name: 'ValidationError'
            }
          }
        });
      }

      if (!user.verified) {
        console.log('Login Error: User is not verified');
        return res.status(401).json({
          errors: {
            password: {
              message: 'validation.password.not_verified',
              name: 'ValidationError'
            }
          }
        });
      }

      jwt.sign(
        {
          user: {
            id: user.id,
            username: user.username
          }
        },
        env.APP_SECRET,
        {
          expiresIn: 10800 // 3h
        },
        (error, token) => {
          if (error) {
            console.log(error);
            return res.status(500).json(error);
          }

          return res.status(200).json({
            message: 'User Logged in successfully',
            name: 'LoginSuccess',
            token
          });
        }
      );
    });
  });
};

// Forgot password
exports.forgotPassword = (req, res) => {
  User.findOne({
    email: req.body.email
  }, function(error, user) {
    if (error) {
      console.log(error);
      return res.status(500).json(error);
    }

    if (!user) {
      console.log('Forgot password Error: e-mail is invalid');
      return res.status(400).json({
        errors: {
          email: {
            message: 'validation.email.invalid',
            name: 'ValidationError'
          }
        }
      });
    }

    if (!user.verified) {
      console.log('Forgot password Error: User is not verified');
      return res.status(401).json({
        errors: {
          email: {
            message: 'validation.email.not_verified',
            name: 'ValidationError'
          }
        }
      });
    }

    jwt.sign(
      {
        user: {
          id: user.id
        }
      },
      env.APP_SECRET,
      {
        expiresIn: 300 // 5 min
      },
      (error, token) => {
        if (error) {
          console.log(error);
          return res.status(500).json(error);
        }

        let mailOptions = {
          from: 'scalenge-noreply@gmail.com',
          to: 'tomasz.ngondo@gmail.com',
          subject: 'Reset password',
          text: req.body.emailMessage.replace('{token}', token).replace('{username}', user.username)
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            return res.status(500).json(error);
          }

          return res.status(200).json({
            message: 'E-mail sent: ' + info.response,
            name: 'ForgotPasswordSuccess',
          });
        });
      }
    );
  });
};

// Edit user
exports.edit = (req, res) => {
  let token = utils.retrieveToken(res, req.headers.authorization);

  try {
    let decoded = jwt.verify(token, env.APP_SECRET);

    User.findOne({
      _id: decoded.user.id
    }, (error, user) => {
      if (error) {
        console.log(error);
        return res.status(500).json(error);
      }

      if (!user.verified) {
        console.log('User edit Error: User is not verified');
        return res.status(401).json({
          errors: {
            password: {
              message: 'validation.password.not_verified',
              name: 'ValidationError'
            }
          }
        });
      }

      for (let key in req.body) {
        if (req.body.hasOwnProperty(key)) {
          user[key] = req.body[key];
        }
      }

      user.save().then(() => {
        jwt.sign(
          {
            user: {
              id: user.id,
              username: user.username
            }
          },
          env.APP_SECRET,
          {
            expiresIn: 10800 // 3h
          },
          (error, token) => {
            if (error) {
              console.log(error);
              return res.status(500).json(error);
            }

            return res.status(200).json({
              message: 'User editted successfully',
              name: 'UserEditSuccess',
              token
            });
          }
        );

      }).catch((error) => {
        console.log(error);
        return res.status(400).json(error);
      });
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: {
        tokenExpired: true,
        message: error
      }
    });
  }
};

// Delete account
exports.deleteAccount = (req, res) => {
  let token = utils.retrieveToken(res, req.headers.authorization);

  try {
    let decoded = jwt.verify(token, env.APP_SECRET);

    User.deleteOne({
      _id: decoded.user.id
    }, function(error, user) {
      if (error) {
        console.log(error);
        return res.status(500).json(error);
      }

      let mailOptions = {
        from: 'scalenge-noreply@gmail.com',
        to: 'tomasz.ngondo@gmail.com',
        subject: 'Delete account',
        text: req.body.emailMessage.replace('{username}', user.username)
      };

      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
          return res.status(500).json(error);
        }

        return res.status(200).json({
          message: 'User successfully deleted, e-mail sent: ' + info.response,
          name: 'DeleteAccountSuccess'
        });
      });
    }).catch((error) => {
      console.log(error);
      return res.status(500).json(error);
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      errors: {
        tokenExpired: true,
        message: error
      }
    });
  }
};

exports.list = (req, res) => {
  User.find({}, function(err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};

exports.get = (req, res) => {
  User.findById(req.params.id, function(err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};

exports.update = (req, res) => {
  User.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, user) {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
};
