const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const UserSchema = new Schema({
  username: {
    type: String,
    required: 'validation.username.required',
    validate: {
      validator: async function(username) {
        const user = await this.constructor.findOne({ username });
        return user ? this.id === user.id : true
      },
      message: props => 'validation.username.exist'
    }
  },
  email: {
    type: String,
    required: 'validation.email.required',
    validate: {
      validator: async function(email) {
        const user = await this.constructor.findOne({ email });
        return user ? this.id === user.id : true
      },
      message: props => 'validation.email.exist'
    },
    match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'validation.email.invalid']
  },
  password: {
    type: String,
    required: 'validation.password.required',
    minlength: [8, 'validation.password.min_length'],
    match: [/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, 'validation.password.constraint']
  },
  verified: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

UserSchema.pre('save', function(next) {
  let user = this;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(10, function (err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.validatePassword = (candidatePassword, password, callback) => {
  bcrypt.compare(candidatePassword, password, (err, isMatch) => {
    if (err) {
      return callback(err);
    } else {
      callback(null, isMatch);
    }
  });
};

module.exports = mongoose.model('User', UserSchema);
