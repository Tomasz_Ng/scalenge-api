module.exports = {
  retrieveToken: (res, token) => {
    if (!token || !token.startsWith('Bearer ')) {
      console.log('Auth Error: token not found');
      return res.status(401).json({ message: 'Auth Error: token not found' });
    }

    return token.slice(7, token.length).trimLeft();
  }
};
