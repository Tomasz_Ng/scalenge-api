module.exports = function(app) {
  const userController = require('../controllers/userController');

  // users Routes
  app.route('/login')
    .post(userController.login);

  app.route('/register')
    .post(userController.register);

  app.route('/register-confirm')
    .post(userController.registerConfirm);

  app.route('/forgot-password')
    .post(userController.forgotPassword);

  app.route('/user/edit')
    .post(userController.edit);

  app.route('/user/delete')
    .delete(userController.deleteAccount);

  app.route('/users')
    .get(userController.list);
};
