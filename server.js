const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');
const env = require('dotenv').config().parsed;
const User = require('./src/models/userModel');
const bodyParser = require('body-parser');
const routes = require('./src/routes/userRoutes');

mongoose.Promise = Promise;

mongoose.connection.on('connected', () => {
  console.log('Connection Established')
});

mongoose.connection.on('reconnected', () => {
  console.log('Connection Reestablished')
});

mongoose.connection.on('disconnected', () => {
  console.log('Connection Disconnected')
});

mongoose.connection.on('close', () => {
  console.log('Connection Closed')
});

mongoose.connection.on('error', (error) => {
  console.log('ERROR: ' + error)
});

const run = async () => {
  await mongoose.connect(env.DATABASE_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  });
};

run().catch(error => console.error(error));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Methods': '*'
  });
  next();
});

routes(app);

app.listen(port);

console.log('user list RESTful API server started on: ' + port);
